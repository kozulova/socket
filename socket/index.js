import * as config from "./config";
import { Room } from '../clases/Room';

const users = [];

const room1 = new Room(1, 'room1', ['t1', 't2']);
const room2 = new Room(2, 'room2', ['t1', 't2']);
let rooms = [room1, room2];

//const getCurrentRoomId = socket => Object.keys(socket.rooms).find(roomId);

export default io => {
  io.on('connection', socket => {
    const username = socket.handshake.query.username;
// Check login //
    socket.on('check', (name, fn) => {
      if (users.indexOf(name) < 0) {
        users.push(name);
        fn('success');
      }
      else {
        fn('error')
      }
    });

    socket.emit("UPDATE_ROOMS", rooms);

    socket.on('ADD_NEW_ROOM', (roomName) => {
      const newRoom = new Room(rooms[rooms.length - 1].id + 1, roomName, [username]);
      rooms.push(newRoom);
      io.emit('UPDATE_ROOMS', rooms);
      console.log('test');
    });

    socket.on('JOIN_ROOM', (roomId)=>{

      socket.join(roomId, () => {
        const room = rooms.find(room=>room.id == roomId);
        room.addUsers(username);
        rooms = rooms.map(r=> r.id == room.id ? room : r);
        //rooms.forEach(room => room.id == newRoom.id ? console.log(true): false);
        console.log(rooms, 'rooms');
        console.log(room, 'room');
        io.to(socket.id).emit("JOIN_ROOM_DONE", room);
      });
    })


    socket.on('START_GAME', (roomId)=>{

    })

    socket.on('disconnect', () => {
      console.log(`${socket.id} is disconnected`);
    })
  })

}