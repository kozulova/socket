import {MAXIMUM_USERS_FOR_ONE_ROOM} from '../socket/config';
const checkRoomIsAvailableToJoin = (roomId, rooms) =>{
    const room = rooms.find(room => {
        room.id === roomId
    });
    return room.users.length <= MAXIMUM_USERS_FOR_ONE_ROOM
}

export{checkRoomIsAvailableToJoin};